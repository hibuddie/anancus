# Copyright 2018 Vi Jay Suskind
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM alpine as backend
RUN apk --no-cache add ca-certificates
RUN apk update
RUN apk add git
RUN apk add go
RUN go get -u github.com/graph-gophers/graphql-go
RUN go get -u github.com/dgrijalva/jwt-go
RUN go get -u golang.org/x/crypto/argon2
RUN go get -u github.com/lib/pq
WORKDIR /app
COPY backend backend
RUN CGO_ENABLED=0 GOOS=linux go build -a -buildmode=exe -installsuffix cgo -o bin/server backend/*.go

FROM alpine as frontend
RUN apk --no-cache add ca-certificates
RUN apk update
RUN apk add nodejs-npm
WORKDIR /app/frontend
COPY frontend/package.json package.json
RUN npm install
COPY frontend/tsconfig.json tsconfig.json
COPY frontend/public public
COPY frontend/src src
RUN npm run build

FROM scratch
WORKDIR /app
COPY --from=backend /app/bin .
COPY --from=backend /app/backend/schema.graphql .
COPY --from=backend /app/backend/graphiql.html .
COPY --from=frontend /app/frontend/dist dist
EXPOSE 8000
ENTRYPOINT [ "/app/server" ]
