// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Signup from './views/Signup.vue'
import Explore from './views/Explore.vue'
import CreatePost from './views/CreatePost.vue'
import ViewPost from './views/ViewPost.vue'
import ViewGroup from './views/ViewGroup.vue'
import Settings from './views/Settings.vue'
import TermsAndConditions from './views/TermsAndConditions.vue'
import PrivacyPolicy from './views/PrivacyPolicy.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'explore',
      component: Explore
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup
    },
    {
      path: '/createPost',
      name: 'createPost',
      component: CreatePost
    },
    {
      path: '/post/:id',
      name: 'viewPost',
      component: ViewPost
    },
    {
      path: '/group/:id',
      name: 'viewGroup',
      component: ViewGroup
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings
    },
    {
      path: '/documents/terms-and-conditions',
      name: 'termsAndConditions',
      component: TermsAndConditions
    },
    {
      path: '/documents/privacy-policy',
      name: 'privacyPolicy',
      component: PrivacyPolicy
    },
  ]
})
