package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
)

func main() {

	getUrl := "http://localhost:8000/activity/person/jess/inbox"
	req, err := http.NewRequest("GET", getUrl, nil)
	if err != nil {
		log.Fatalf("could not get inbox:", err)
	}
	req.Header.Set("Accept", "application/activity+json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("could not request:", err)
	}
	defer resp.Body.Close()
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		log.Fatalf("could not parse response body:", err)
	}
	fmt.Println(string(dump))
}
