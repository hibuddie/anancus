CREATE TABLE IF NOT EXISTS persons (
	id		SERIAL,
	name		TEXT NOT NULL,
	password	TEXT NOT NULL,
	public_key	TEXT NOT NULL,
	private_key	TEXT NOT NULL,
	fingerprint	TEXT NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS groups (
	id	SERIAL,
	name	TEXT NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS documents (
	id		SERIAL,
	url		TEXT NOT NULL,
	name		TEXT NOT NULL,
	author		TEXT NOT NULL,
	created_at	TIMESTAMP NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS liked (
	id		SERIAL,
	author		TEXT NOT NULL,
	uri		TEXT NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS person_followers (
	id		SERIAL,
	person		TEXT NOT NULL,
	follower	TEXT NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS group_members (
	id		SERIAL,
	uri		TEXT NOT NULL,
	member		TEXT NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS document_groups (
	id		SERIAL,
	document	INTEGER NOT NULL,
	uri		TEXT NOT NULL,
	FOREIGN KEY (document) REFERENCES documents (id),
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS comments (
	id		SERIAL,
	document 	INTEGER NOT NULL,
	author		TEXT NOT NULL,
	parent		INTEGER,
	comment		TEXT NOT NULL,
	depth		INTEGER NOT NULL,
	created_at	TIMESTAMP NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (document) REFERENCES documents (id),
	FOREIGN KEY (parent) REFERENCES comments (id)
);

