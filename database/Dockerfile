# Copyright 2018 Vi Jay Suskind
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM alpine:latest as certificates
RUN apk --update add ca-certificates

FROM alpine as builder
RUN wget --quiet https://binaries.cockroachdb.com/cockroach-v2.0.3.linux-musl-amd64.tgz -O /tmp/cockroach.tgz && \
    tar xvzf /tmp/cockroach.tgz --strip 1

FROM scratch
ENV COCKROACH_CHANNEL=official-docker
COPY --from=certificates /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /cockroach /cockroach/cockroach
WORKDIR /cockroach/
EXPOSE 26257
EXPOSE 8080
ENTRYPOINT ["/cockroach/cockroach"]

