// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/spf13/viper"
)

const (
	version = "v0.0.1"
)

var (
	baseUrl string
)

func main() {
	viper.SetConfigType("toml")
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	baseUrl = fmt.Sprintf("%s://%s:%d", viper.GetString("server.scheme"), viper.GetString("server.hostname"), viper.GetInt("server.port"))

	initDB()
	defer closeDB()

	initApplicationActor()
	initPersonActors()
	initGroupActors()

	router := createRouter()

	log.Println("Starting server...")

	serverHostname := viper.GetString("server.hostname")
	serverPort := viper.GetInt("server.port")
	addr := serverHostname + ":" + strconv.Itoa(serverPort)
	s := http.Server{
		Addr:         addr,
		Handler:      router,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	go func() {
		err := s.ListenAndServe()
		if err != nil {
			log.Println("ListenAndServe: ", err)
		}
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)
	<-stop

	log.Println("Shutdown signal received, exiting...")

	s.Shutdown(context.Background())
}
