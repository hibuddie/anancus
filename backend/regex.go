// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"regexp"
)

// Regexes for request path matching
// =================================

// Actor
var applicationRegexActor = regexp.MustCompile("^/activity/application/?$")
var personRegexActor = regexp.MustCompile("^/activity/person/([^/]+)/?$")
var groupRegexActor = regexp.MustCompile("^/activity/group/([^/]+)/?$")

// Inbox
var applicationRegexInbox = regexp.MustCompile("^/activity/application/inbox/?$")
var personRegexInbox = regexp.MustCompile("^/activity/person/([^/]+)/inbox/?$")
var groupRegexInbox = regexp.MustCompile("^/activity/group/([^/]+)/inbox/?$")

// Outbox
var applicationRegexOutbox = regexp.MustCompile("^/activity/application/outbox/?$")
var personRegexOutbox = regexp.MustCompile("^/activity/person/([^/]+)/outbox/?$")
var groupRegexOutbox = regexp.MustCompile("^/activity/group/([^/]+)/outbox/?$")

// Following
var applicationRegexFollowing = regexp.MustCompile("^/activity/application/following/?$")
var personRegexFollowing = regexp.MustCompile("^/activity/person/([^/]+)/following/?$")
var groupRegexFollowing = regexp.MustCompile("^/activity/group/([^/]+)/following/?$")

// Followers
var applicationRegexFollowers = regexp.MustCompile("^/activity/application/followers/?$")
var personRegexFollowers = regexp.MustCompile("^/activity/person/([^/]+)/followers/?$")
var groupRegexFollowers = regexp.MustCompile("^/activity/group/([^/]+)/followers/?$")

// Liked
var applicationRegexLiked = regexp.MustCompile("^/activity/application/liked/?$")
var personRegexLiked = regexp.MustCompile("^/activity/person/([^/]+)/liked/?$")
var groupRegexLiked = regexp.MustCompile("^/activity/group/([^/]+)/liked/?$")

var documentRegex = regexp.MustCompile("^/document/?$")
var createRegex = regexp.MustCompile("^/create/?$")
var likedRegex = regexp.MustCompile("^/liked/?$")
var joinRegex = regexp.MustCompile("^/join/?$")
var followRegex = regexp.MustCompile("^/follow/?$")
