// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/spf13/viper"
)

func createRouter() http.Handler {
	pubber, asHandler := createPubber()
	inboxHandler := func(w http.ResponseWriter, r *http.Request) {
		c := context.Background()
		if handled, err := pubber.PostInbox(c, w, r); err != nil {
			w.Header().Set("Content-Type", "text/plain")
			log.Println("Error in PostInbox", err)
			w.Write([]byte("Error in PostInbox.\n"))
		} else if handled {
			return
		}
		if handled, err := pubber.GetInbox(c, w, r); err != nil {
			w.Header().Set("Content-Type", "text/plain")
			log.Println("Error in GetInbox", err)
			w.Write([]byte("Error in GetInbox.\n"))
		} else if handled {
			return
		}
		w.Header().Set("Content-Type", "text/plain")
		w.Write([]byte("Inbox default.\n"))
	}

	outboxHandler := func(w http.ResponseWriter, r *http.Request) {
		c := context.Background()
		if handled, err := pubber.PostOutbox(c, w, r); err != nil {
			w.Header().Set("Content-Type", "text/plain")
			log.Println("Error in PostOutbox", err)
			w.Write([]byte("Error in PostOutbox.\n"))
		} else if handled {
			return
		}
		if handled, err := pubber.GetOutbox(c, w, r); err != nil {
			w.Header().Set("Content-Type", "text/plain")
			log.Println("Error in GetOutbox", err)
			w.Write([]byte("Error in GetOutbox.\n"))
		} else if handled {
			return
		}
		w.Header().Set("Content-Type", "text/plain")
		w.Write([]byte("Outbox default.\n"))
	}

	actorHandler := func(w http.ResponseWriter, r *http.Request) {
		c := context.Background()
		if handled, err := asHandler(c, w, r); err != nil {
			w.Header().Set("Content-Type", "text/plain")
			log.Println("Error in asHandler", err)
			w.Write([]byte("Error in ActivityStreams.\n"))
		} else if handled {
			return
		}
		w.Header().Set("Content-Type", "text/plain")
		w.Write([]byte("ActivityStreams default.\n"))
	}

	router := mux.NewRouter()

	router.HandleFunc("/activity/application/inbox", inboxHandler)
	router.HandleFunc("/activity/person/{actor}/inbox", inboxHandler)

	router.HandleFunc("/activity/application/outbox", outboxHandler)
	router.HandleFunc("/activity/person/{actor}/outbox", outboxHandler)

	router.HandleFunc("/activity/application", actorHandler)
	router.HandleFunc("/activity/person/{actor}", actorHandler)

	graphiql, err := ioutil.ReadFile("graphiql.html")
	if err != nil {
		panic(err)
	}
	router.HandleFunc("/graphiql", func(w http.ResponseWriter, r *http.Request) {
		w.Write(graphiql)
	})

	b, err := ioutil.ReadFile(viper.GetString("graphql.file"))
	if err != nil {
		panic(err)
	}
	schema := graphql.MustParseSchema(string(b), &Resolver{})
	router.Handle("/graphql", auth(&relay.Handler{Schema: schema}))

	router.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	router.HandleFunc("/version", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, version)
	})

	router.PathPrefix("/").Handler(http.FileServer(http.Dir(viper.GetString("server.dist"))))

	return router
}
