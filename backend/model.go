// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"net/url"
	"strings"

	"database/sql"
	"github.com/go-fed/activity/vocab"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

var db *sql.DB

func initDB() {
	dbstrs := []string{
		"host=" + viper.GetString("database.host"),
		"dbname=" + viper.GetString("database.name"),
		"sslmode=" + viper.GetString("database.sslmode"),
		"user=" + viper.GetString("database.user"),
	}
	dbstr := strings.Join(dbstrs, " ")

	var err error
	db, err = sql.Open("postgres", dbstr)
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}
}

func initApplicationActor() {
	applicationActor = &applicationActorType{}
	applicationActor.actor = &vocab.Application{}

	applicationActor.actorMu.Lock()
	defer applicationActor.actorMu.Unlock()

	inbox := &vocab.OrderedCollection{}
	u, err := url.Parse(fmt.Sprintf("%s/activity/application/inbox", baseUrl))
	if err != nil {
		panic(err)
	}
	inbox.SetId(u)

	outbox := &vocab.OrderedCollection{}
	u, err = url.Parse(fmt.Sprintf("%s/activity/application/outbox", baseUrl))
	if err != nil {
		panic(err)
	}
	outbox.SetId(u)

	following := &vocab.Collection{}
	u, err = url.Parse(fmt.Sprintf("%s/activity/application/following", baseUrl))
	if err != nil {
		panic(err)
	}
	following.SetId(u)

	followers := &vocab.Collection{}
	u, err = url.Parse(fmt.Sprintf("%s/activity/application/followers", baseUrl))
	if err != nil {
		panic(err)
	}
	followers.SetId(u)

	liked := &vocab.Collection{}
	u, err = url.Parse(fmt.Sprintf("%s/activity/application/liked", baseUrl))
	if err != nil {
		panic(err)
	}
	liked.SetId(u)

	u, err = url.Parse(fmt.Sprintf("%s/activity/application", baseUrl))
	if err != nil {
		panic(err)
	}
	applicationActor.actor.SetId(u)
	applicationActor.actor.SetInboxOrderedCollection(inbox)
	applicationActor.actor.SetOutboxOrderedCollection(outbox)
	applicationActor.actor.SetFollowingCollection(following)
	applicationActor.actor.SetFollowersCollection(followers)
	applicationActor.actor.SetLikedCollection(liked)
}

func initPersonActors() {
	sql := "SELECT name FROM persons;"
	rows, err := db.Query(sql)
	defer rows.Close()
	if err != nil {
		panic(err)
	}
	personActors = make(map[string]*personActorType)
	for rows.Next() {
		var name string
		err = rows.Scan(&name)
		if err != nil {
			panic(err)
		}

		personActors[name] = &personActorType{}

		personActors[name].actorMu.Lock()

		personActors[name].actor = &vocab.Person{}

		inbox := &vocab.OrderedCollection{}
		u, err := url.Parse(fmt.Sprintf("%s/activity/person/%s/inbox", baseUrl, name))
		if err != nil {
			panic(err)
		}
		inbox.SetId(u)

		outbox := &vocab.OrderedCollection{}
		u, err = url.Parse(fmt.Sprintf("%s/activity/person/%s/outbox", baseUrl, name))
		if err != nil {
			panic(err)
		}
		outbox.SetId(u)

		following := &vocab.Collection{}
		u, err = url.Parse(fmt.Sprintf("%s/activity/person/%s/following", baseUrl, name))
		if err != nil {
			panic(err)
		}
		following.SetId(u)

		followers := &vocab.Collection{}
		u, err = url.Parse(fmt.Sprintf("%s/activity/person/%s/followers", baseUrl, name))
		if err != nil {
			panic(err)
		}
		followers.SetId(u)

		liked := &vocab.Collection{}
		u, err = url.Parse(fmt.Sprintf("%s/activity/person/%s/liked", baseUrl, name))
		if err != nil {
			panic(err)
		}
		liked.SetId(u)

		u, err = url.Parse(fmt.Sprintf("%s/activity/person/%s", baseUrl, name))
		if err != nil {
			panic(err)
		}
		personActors[name].actor.SetId(u)
		personActors[name].actor.SetInboxOrderedCollection(inbox)
		personActors[name].actor.SetOutboxOrderedCollection(outbox)
		personActors[name].actor.SetFollowingCollection(following)
		personActors[name].actor.SetFollowersCollection(followers)
		personActors[name].actor.SetLikedCollection(liked)

		personActors[name].actorMu.Unlock()
	}
}

func initGroupActors() {
	sql := "SELECT name FROM groups;"
	rows, err := db.Query(sql)
	defer rows.Close()
	if err != nil {
		panic(err)
	}
	groupActors = make(map[string]*groupActorType)
	for rows.Next() {
		var name string
		err = rows.Scan(&name)
		if err != nil {
			panic(err)
		}

		groupActors[name] = &groupActorType{}

		groupActors[name].actorMu.Lock()

		groupActors[name].actor = &vocab.Group{}

		inbox := &vocab.OrderedCollection{}
		u, err := url.Parse(fmt.Sprintf("%s/activity/group/%s/inbox", baseUrl, name))
		if err != nil {
			panic(err)
		}
		inbox.SetId(u)

		outbox := &vocab.OrderedCollection{}
		u, err = url.Parse(fmt.Sprintf("%s/activity/group/%s/outbox", baseUrl, name))
		if err != nil {
			panic(err)
		}
		outbox.SetId(u)

		following := &vocab.Collection{}
		u, err = url.Parse(fmt.Sprintf("%s/activity/group/%s/following", baseUrl, name))
		if err != nil {
			panic(err)
		}
		following.SetId(u)

		followers := &vocab.Collection{}
		u, err = url.Parse(fmt.Sprintf("%s/activity/group/%s/followers", baseUrl, name))
		if err != nil {
			panic(err)
		}
		followers.SetId(u)

		liked := &vocab.Collection{}
		u, err = url.Parse(fmt.Sprintf("%s/activity/group/%s/liked", baseUrl, name))
		if err != nil {
			panic(err)
		}
		liked.SetId(u)

		u, err = url.Parse(fmt.Sprintf("%s/activity/group/%s", baseUrl, name))
		if err != nil {
			panic(err)
		}
		groupActors[name].actor.SetId(u)
		groupActors[name].actor.SetInboxOrderedCollection(inbox)
		groupActors[name].actor.SetOutboxOrderedCollection(outbox)
		groupActors[name].actor.SetFollowingCollection(following)
		groupActors[name].actor.SetFollowersCollection(followers)
		groupActors[name].actor.SetLikedCollection(liked)

		groupActors[name].actorMu.Unlock()
	}
}

func closeDB() {
	db.Close()
}
